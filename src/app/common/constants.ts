export const DEFAULTS = {
    apiURL : 'http://api.openweathermap.org/data/2.5/',
    apiKey : '3d8b309701a13f65b660fa2c64cdc517',
}

export const Images = {
    Rain:'../../assets/images/Rainy.jpg',
    Clouds:'../../assets/images/Cloudy.jpg',
    Clear:'../../assets/images/Clear.jpg',
}