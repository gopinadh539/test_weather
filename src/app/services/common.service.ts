import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DEFAULTS } from '../common/constants';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class CommonService {

  constructor(private httpClient: HttpClient) { }
  private detailsObj$: BehaviorSubject<any> = new BehaviorSubject(null);

  getProfileObs(): Observable<any> {
    return this.detailsObj$.asObservable();
  }

  setProfileObs(cityData: any) {
    this.detailsObj$.next(cityData);
  }

  public getReportBasedOnWhether(cityName: string) {
    return this.httpClient.get(`${DEFAULTS.apiURL}/weather?q=${cityName}&appid=${DEFAULTS.apiKey}`);
  }

  public getWhetherReportForMultipleCities() {
    return this.httpClient.get(`${DEFAULTS.apiURL}/box/city?bbox=12,32,15,37,10&appid=${DEFAULTS.apiKey}`);
  }
}

