import { Component, OnInit } from '@angular/core';
import { CommonService } from '../services/common.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Images } from '../common/constants';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit {
  whetherData: any = [];
  images = Images;
  constructor(
    private commonService: CommonService,
    private router: Router) { }

  ngOnInit() {
    this.commonService.getWhetherReportForMultipleCities().subscribe(data => {
      this.whetherData = data['list'].slice(10, 15);
    }, (err) => {
      this.handleError(err);
    })
  }

  showDetails(item) {
    this.commonService.setProfileObs(item);
    this.router.navigate(['/details', item.name]);
  }

  handleError(err) {
    console.log('Error');
  }

}
