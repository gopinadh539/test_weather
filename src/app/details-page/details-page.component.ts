import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CommonService } from '../services/common.service';
import { Images } from '../common/constants';

@Component({
  selector: 'app-details-page',
  templateUrl: './details-page.component.html',
  styleUrls: ['./details-page.component.scss']
})
export class DetailsPageComponent implements OnInit {
  cityWhetherData: any;
  selectedCityName: String;
  images = Images;
  constructor(private route: ActivatedRoute, private commonService: CommonService) { }

  ngOnInit() {
    this.commonService.getProfileObs().subscribe(data => {
      if (data) {
        this.cityWhetherData = data;
      } else {
        this.getDataFromService();
      }
    });
  }

  getDataFromService() {
    this.route.params.subscribe(params => {
      if (params) {
        this.commonService.getReportBasedOnWhether(params.name).subscribe(details => {
          this.cityWhetherData = details;
        }, (err) => {
          this.handleError(err);
        });
      }
    });
  }

  handleError(err) {
    console.log('Error', err);
  }

}
